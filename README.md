# Einfacher LoRaWAN Node für das TTN auf Basis des STM32

Die notwendige Umgebung für dieses Repo ist die Arduino IDE mit der STM32duino Erweiterung von hier: https://github.com/rogerclarkmelbourne/Arduino_STM32

- *First_STM32_OTAA_Test* beinhaltet einen einfachen Test um via OTAA mit dem TTN zu reden 
- *TomTor_Version_STM32_OTAA* beinhaltet die optimierte OTAA Version von TomTor aus dieser Quelle: https://gist.github.com/tomtor/408dad505fd078b434bbf2e86bb88a5e

Weitere Infos und Support gibt es unter den folgenden Adressen

Webseite: https://iot-usergroup.de

Forum   : https://forum.iot-usergroup.de

Wiki    : https://wiki.iot-usergroup.de

